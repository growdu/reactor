#ifndef GLOBAL_H
#define GLOBAL_H

typedef void *(*FUNC) (void *arg);
typedef struct _thpool_job_t thpool_job_t;
typedef struct _thpool_jobqueue thpool_jobqueue;
typedef struct _thpool_t thpool_t;
typedef struct reactor reactor_t;
typedef struct reactor_core reactor_core_t;
typedef struct event_handler event_handler_t;
typedef struct handle_event_msg handle_event_msg_t;

#endif
