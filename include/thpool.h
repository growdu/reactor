#ifndef THPOOL_H
#define THPOOL_H
#include <semaphore.h>

typedef void *(*FUNC) (void *arg);
typedef struct _thpool_job_t thpool_job_t;
typedef struct _thpool_jobqueue thpool_jobqueue;
typedef struct _thpool_t thpool_t;

thpool_t *thpool_init(int threadN);

struct _thpool_t{
	pthread_t* threads;
	int threadsN;
	thpool_jobqueue* jobqueue;
};

struct _thpool_jobqueue{
	thpool_job_t *head;
	thpool_job_t *tail;
	int jobN;
	sem_t *queueSem;
};

struct _thpool_job_t{
	FUNC function;
	void *arg;
	struct _thpool_job_t *prev;
	struct _thpool_job_t *next;
};


#endif
