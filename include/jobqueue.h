#ifndef JOBQUEUE_H
#define JOBQUEUE_H

typedef struct event_handler event_handler_t;
typedef struct handle_event_msg handle_event_msg_t;

void thpool_jobqueue_init(thpool_t *thpool);
void *job_handle(void *arg);
int insert_jobqueue(thpool_t *thpool,event_handler_t *eh,handle_event_msg_t arg);
thpool_job_t *remove_jobqueue(thpool_t *);

#endif
