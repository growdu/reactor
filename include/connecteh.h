#ifndef CONNECTEH_H
#define CONNECTEH_H

#define BUF_SIZE 1024

event_handler_t *create_connecteh(int connfd,reactor_t *reactor);
static void *handle_event(void *msg);

#endif
