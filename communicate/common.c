#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "common.h"

void outputError(int err,const char *format,...){
	int savedErrno;
	va_list argList;
	char userMsg[USER_ERR_BUF];
	char errMsg[ERR_BUF];
	char errText[ERR_TEXT_BUF];

	savedErrno = err;

	va_start(argList,format);
	vsnprintf(userMsg,USER_ERR_BUF,format,argList);
	va_end(argList);

	strerror_r(err,errMsg,ERR_BUF);

	snprintf(errText,ERR_TEXT_BUF,"ERROR [%s] %s\n",errMsg,userMsg);
	fputs(errText,stderr);
	fflush(stderr);

	err = savedErrno;

	exit(EXIT_FAILURE);
}
