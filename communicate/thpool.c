#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdlib.h>
#include <errno.h>
#include "thpool.h"
#include "jobqueue.h"

thpool_t *thpool_init(int threadN){
	thpool_t *thpool;	
	int i;

	thpool = (thpool_t *)malloc(sizeof(thpool_t));
	if(thpool == NULL){
		outputError(errno,"%s","malloc thpool_t");
	}

	thpool->threadsN = threadN;
	thpool->threads = (pthread_t *)malloc(threadN * sizeof(pthread_t));

	thpool_jobqueue_init(thpool);

	for(i = 0; i < threadN; i++){
		pthread_create(&thpool->threads[i],NULL,job_handle,thpool);
	}

	return thpool;
}

