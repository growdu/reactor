### 一个用c语言实现的简易reactor模型。

 **模型简介：** 

reactor模型的概念和种类请参考如下链接[https://cloud.tencent.com/developer/article/1488120](http://)，此reactor模型是单Reactor多线程类型。

 **代码展示及主要功能：** 

 **原有文件目录：** 

- communicate目录:（包含源文件）
- include目录:(包含头文件)
- Makefile文件

 **make后创建的文件目录：** 

- bin:（可执行的目标文件）
- dep:（Makefile自动生成依赖的中间文件）
- obj:（可重定位的目标文件）

 **运行方式：** 
- make
- make DEBUG=y （编译参数加上 -g，用于调试）
- make clean （清除编译）


